package bookstoread;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

import static org.assertj.core.api.Assertions.*;

/**
 * @author Mohamed Traore
 */
class BookShelfSpec {

    private BookShelf shelf;
    private Book effectiveJava;
    private Book codeComplete;
    private Book mythicalManMonth;

    @BeforeEach
    void init() throws Exception {
        shelf = new BookShelf();
        effectiveJava = new Book("Effective Java", "Joshua Bloch", LocalDate.of(2008, Month.MAY, 8));
        codeComplete = new Book("Code Complete", "Steve McConnel", LocalDate.of(2004, Month.JUNE, 9));
        mythicalManMonth = new Book("The Mythical Man-Month", "Frederick Phillips Brooks", LocalDate.of(1975, Month.JANUARY, 1));
    }

    @Test
    void shelfEmptyWhenNoBookAdded() throws Exception {
//        BookShelf shelf = new BookShelf();
//        List<String> books = shelf.books();
//        assertTrue(books.isEmpty(), "BookShelf should be empty");
          List<Book> books =  shelf.books();
          assertTrue(books.isEmpty(), "BookShelf should be empty");
    }

    @Test
    void bookShelfContainsTwoBooksWhenTwoBooksAdded() {
//        BookShelf shelf = new BookShelf();
//        shelf.add("Effective Java", "Code complete");
//        shelf.add();
//        List<String> books = shelf.books();
//        assertEquals(2, books.size(), "BookShelf should have two books.");
        shelf.add(effectiveJava, codeComplete);
        List<Book> books = shelf.books();
        assertEquals(2, books.size(), "BookShelf should have two books");
    }

    @Test
    void emptyBookShelfWhenAddIsCalledWithoutBooks() {
//        BookShelf shelf = new BookShelf();
//        shelf.add();
//        List<String> books = shelf.books();
//        assertTrue(books.isEmpty(), "BookShelf should be empty");
        shelf.add();
        List<Book> books = shelf.books();
        assertTrue(books.isEmpty(), "BookShelf should be empty");
    }

    @Test
    void booksReturnedFromShelfIsImmutableForClient() {
//        BookShelf shelf = new BookShelf();
//        shelf.add("Effective Java", "Code complete");
//        List<String> books = shelf.books();
//        try {
//            books.add("The Mythical Man-Month");
//            fail("Should not able to add book to books");
//        } catch (Exception e) {
//            assertTrue(e instanceof UnsupportedOperationException, "Should throw UnsupportedOperationException.");
//        }
        shelf.add(effectiveJava, codeComplete);
        List<Book> books = shelf.books();
        try {
            books.add(mythicalManMonth);
//            fail("Should not be able to add book to books");
        } catch (Exception e) {
            assertTrue(e instanceof UnsupportedOperationException, "Should throw UnsupportedOperationException");
        }
    }

//    @Disabled("Needs to implements Comparator")
    @DisplayName("bookshelf is arranged lexicographically by book title")
    @Test
    void bookshelfArrangedByBookTitle() {
//        shelf.add("Effective Java", "Code Complete", "The Mythical Man-Month");
//        List<String> books = shelf.arrange();
//        assertEquals(Arrays.asList("Code Complete", "Effective Java", "The Mythical Man-Month"), books,
//                "Books in a bookshelf should be arranged lexicographycally by book title");
        shelf.add(effectiveJava, codeComplete, mythicalManMonth);
        List<Book> books = shelf.arrange();
        assertEquals(Arrays.asList(codeComplete, effectiveJava, mythicalManMonth), books,
                "Books in a bookshelf should be arranged lexicographycally by book title");
    }

    @Test
    void booksInBookShelfAreInInsertionOrderAfterCallingArrange() {
//        shelf.add("Effective Java", "Code Complete", "The Mythical Man-Month");
//        shelf.arrange();
//        List<String> books = shelf.books();
//        assertEquals(Arrays.asList("Effective Java", "Code Complete", "The Mythical Man-Month"), books,
//                "Books in bookshelf are in insertion order");
        shelf.add(effectiveJava, codeComplete, mythicalManMonth);
        List<Book> books = shelf.arrange();
        assertEquals(Arrays.asList(codeComplete, effectiveJava, mythicalManMonth), books,
                "Books in bookshelf are in insertion order");
    }

    @Test
    void bookshelfArrangedByUserProvidedCriteria() {
        shelf.add(effectiveJava, codeComplete, mythicalManMonth);
        List<Book> books = shelf.arrange(Comparator.<Book>naturalOrder().reversed());
        assertEquals(Arrays.asList(mythicalManMonth, effectiveJava, codeComplete), books,
                "Books in a bookshelf are arranged in descending order of book title");
        Comparator<Book> reversed = Comparator.<Book>naturalOrder().reversed();
        shelf.arrange(reversed);
        assertThat(books).isSortedAccordingTo(reversed);
    }
}
